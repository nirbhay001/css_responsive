## In this project, I have made a Team-Page project by using:
* HTML
* CSS
## Project Description
* In this project, I have made a Team-Page Project using HTML and CSS with a beautiful layout and user interface.
* I have used CSS properties like flex and grid etc.
## Visit this site to see the user-interface
* https://master--prismatic-caramel-52a007.netlify.app/